/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ["**/**/*.test.ts"],
  // verbose: true,
  forceExit: true,
  coverageDirectory: "coverage",
  coverageProvider: "v8",
  coverageReporters: ["text", "lcov", "clover", "json-summary", "html"],
  coverageThreshold: {
    global: {
      branches: 50,
      functions: 25,
      lines: 50,
      statements: 50,
    },
  },
  collectCoverageFrom:[
    "src/**/*.ts",
    "!src/controllers/**",
    '!**/node_modules/**',
    '!**/vendor/**',
    '!**/coverage/**',
    '!**/dist/**',
    '!**/test/**',
    "!src/index.ts"
  ]
};