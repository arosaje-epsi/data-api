import multer from 'multer';
import path from 'path';

const authorizedMimeTypes = [
  'jpg',
  'jpeg',
  'png',
  'jfif',
  'webp',
  'bmp',
  'gif'
]

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/uploads')
    },

    filename: (req, file, cb) => {
        cb(null, Date.now() + "-" + file.originalname.split(' ').join('_'))
    }
})

const limits = { fileSize: 1024 * 1024 * 5 }

const fileFilter = (req: any, file: any, cb: any) => {
  if (!authorizedMimeTypes.includes(file.mimetype.split('/')[1].toLowerCase())) {
    return cb(new Error('File type is not supported : ' + file.mimetype), false)
  }
  cb(null, true)
}

module.exports = multer({ storage: storage, limits: limits, fileFilter: fileFilter })