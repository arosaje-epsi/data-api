import dotenv from 'dotenv';
import path from 'path';
dotenv.config({ path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`)});

export const addUserFromJwtToReq = async (req: any, res: any, next: any) => {
	const jwtCookie = req.cookies.jwt;
  
  if(jwtCookie){
    const checkedToken = await fetch(`${process.env.AUTH_API_URL}/checkToken`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ jwt: jwtCookie }),
    }).then((res: any) => res.json());
  
    if (checkedToken.data && checkedToken.data.user) {
      req.user = checkedToken.data.user;
    }else{
      req.expiredToken = true
    }
  }

  next()
	
};

export const isAuthenticated = async (req: any, res: any, next: any) => {
  if(req.user) {
    next()
  }else if(req.expiredToken){
    return res.status(401).clearCookie("jwt").json({message:"Unauthorized"})
  } else {
    return res.status(401).json({message: "Unauthorized"})
  }
}
