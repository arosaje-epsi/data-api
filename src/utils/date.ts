export function convertDateToISO(dateString: string): string {
  const [day, month, year] = dateString.split('-').map(Number);
  const date = new Date(`${year}-${month}-${day} 15:00:00`);
  return date.toISOString()
}