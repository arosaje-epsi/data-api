import express from 'express';
import cors from 'cors';
import { addUserFromJwtToReq } from './utils/jwt';
import path from 'path';

export const app = express();
const router = express.Router();
const apiRoutes = require("./routes/apiRoutes");
const cookieParser = require('cookie-parser')

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

app
  .use(cors({
    origin: ['http://localhost:4200', 'http://127.0.0.1:4200', 'http://127.0.0.1:3000', 'http://localhost:3000'],
    credentials: true
  }))
  .use(express.json())
  .use(express.urlencoded({ extended: true }))
  .use(cookieParser())
  .use(addUserFromJwtToReq)
  .use('/uploads', express.static(path.join(__dirname, '../public/uploads')))
  .use(router);

router.use("/api", apiRoutes)

export default app