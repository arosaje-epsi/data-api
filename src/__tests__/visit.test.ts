import supertest from "supertest";
import app from "../app";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
const jwt = require("jsonwebtoken");
const jwtSecretKey = process.env.JWT_SECRET_KEY;
import * as fs from "fs";
import axios from "axios";

import dotenv from 'dotenv';
import path from 'path';
dotenv.config({ path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`)});


describe("Visit", () => {

  let user;
  let userToken;
  let guard;
  
  describe("Given user add a visit to a guard", () => {
    
    beforeEach(async () => {
    
      user = await prisma.user.create({
        data: {
          role: "user",
          firstname: "John",
          lastname: "Doe",
          email: "test@visit.com",
          password: "test",
        }
      })
  
      userToken = jwt.sign(
        {
          sub: user.id,
          exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
        },
        jwtSecretKey
      )
  
      guard = await prisma.guard.create({
        data: {
          startDate: new Date(),
          endDate: new Date(),
          address: "1 rue de la paix",
          city: "Paris",
          zipCode: "75000",
          owner: {
            connect: {
              id: user.id
            }
          },
          guardian: {
            connect: {
              id: user.id
            }
          },
          plants: {
            create: [
              {
                name: "Tomate",
                type: 'Arbres',
                image: "https://i.imgur.com/6VBx3io.png",
              }
            ]
          
          }
        }
      })
    })
  
    afterEach(async () => {

      const visit = await prisma.visit.findFirst({
        where: {
          guardId: guard!.id
        },
        include: {
          plants: true
        }
      })

      if(visit){
        const imageName = visit?.plants[0].image
  
        if(fs.existsSync(`public/uploads/${imageName!}`)) {
          fs.unlinkSync(`public/uploads/${imageName!}`)
        }

        await prisma.plantVisit.deleteMany({
          where: {
            visitId: visit!.id
          }
        })
      }

      
  
      await prisma.visit.deleteMany({
        where:{
          guardId: guard!.id
        }
      })

      await prisma.plant.deleteMany({
        where: {
          guardId: guard!.id
        }
      })
  
      await prisma.guard.deleteMany({
        where: {
          ownerId: user!.id
        }
      })
  
      await prisma.user.deleteMany({
        where: {
          id: user!.id
        }
      })
  
    })

    it("should fail if date is missing", async () => {

      await supertest(app)
        .post(`/api/visit/add/${guard!.id}`)
        .set("Cookie", [`jwt=${userToken!}`])
        .send({
          comment: "This is a test comment",
          plants: []
        })
        .expect(400)
        .then((response) => {
          expect(response.body.message).toBe("Date is required")
        })

    })

    it("should fail if user not authenticated", async () => {

      await supertest(app)
        .post(`/api/visit/add/${guard!.id}`)
        .send({
          date: "01-01-2021",
          comment: "This is a test comment",
          plants: []
        })
        .expect(401)
        .then((response) => {
          expect(response.body.message).toBe("Unauthorized")
        })

    })



  })
})