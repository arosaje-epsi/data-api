import supertest from "supertest";
import app from "../app";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
const jwt = require("jsonwebtoken");
const jwtSecretKey = process.env.JWT_SECRET_KEY;
import { describe } from "node:test";

import dotenv from 'dotenv';
import path from 'path';
dotenv.config({ path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`)});

describe("Advices", () => {

  let botanist;
  let botanistToken;
  let guard;
  let advice;

  describe("Given botanist add an advice to a guard", () => {

    beforeEach(async () => {
      // Create a botanist and a guard
      advice = undefined;
      
      botanist = await prisma.user.create({
        data: {
          role: "botanist",
          firstname: "Botanist",
          lastname: "Botanist",
          email: "botanist@advice.com",
          password: "botanist",
          status:"accepted"
        }
      });

      botanistToken = jwt.sign(
        {
					sub: botanist.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
      )

      guard = await prisma.guard.create({
        data: {
          startDate: new Date(),
          endDate: new Date(),
          address: "1 rue de la paix",
          city: "Paris",
          zipCode: "75000",
          owner: {
            connect: {
              id: botanist.id
            }
          },
        }
      })

    })

    afterEach(async () => {

      
      await prisma.advice.deleteMany({
        where: {
          guardId: guard!.id
        }
      })
      

      await prisma.guard.delete({
        where: {
          id: guard!.id
        }
      })

      await prisma.user.delete({
        where: {
          id: botanist!.id
        }
      })

    })

    it("Should add an advice to a guard", async () => {
        
        await supertest(app)
          .post(`/api/advice/guard/add`)
          .send({
            guardId: guard!.id,
            content: "This is a test advice"
          })
          .set("Cookie", [`jwt=${botanistToken!}`])
          .expect(201)
          .expect((res) => {
            advice = res.body.data.advice
            expect(res.body.message).toEqual("Advice added")
            expect(res.body.data.advice.content).toEqual("This is a test advice")
          })

  

    })

    it("Should fail if guardId is missing", async () => {

      await supertest(app)
        .post(`/api/advice/guard/add`)
        .send({
          content: "This is a test advice"
        })
        .set("Cookie", [`jwt=${botanistToken!}`])
        .expect(400)
        .expect((res) => {
          expect(res.body.error).toEqual("Missing required fields")
        })

    })

    it("Should fail if content is missing", async () => {

      await supertest(app)
        .post(`/api/advice/guard/add`)
        .send({
          guardId: guard!.id,
        })
        .set("Cookie", [`jwt=${botanistToken!}`])
        .expect(400)
        .expect((res) => {
          expect(res.body.error).toEqual("Missing required fields")
        })

    })

    it("Should fail if botanist is not authenticated", async () => {

      await supertest(app)
        .post(`/api/advice/guard/add`)
        .send({
          guardId: guard!.id,
          content: "This is a test advice"
        })
        .expect(401)
        .expect((res) => {
          expect(res.body.message).toEqual("Unauthorized")
        })

    })

  })
})