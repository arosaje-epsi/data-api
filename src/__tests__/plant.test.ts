import supertest from "supertest";
import app from "../app";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();



describe("Plant", () => {
  describe("Given we are getting plant types", () => {
    it("should return all plant types", async () => {

      const plantTypes = await prisma.plantType.findMany();

      const plantTypesNumber = plantTypes.length;

      await supertest(app)
        .get("/api/plant/getTypes")
        .expect(200)
        .expect((res) => {
          expect(res.body.length).toEqual(plantTypesNumber);
        });
    })
  })
})