import supertest from "supertest";
import app from "../app";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
const jwt = require("jsonwebtoken");
const jwtSecretKey = process.env.JWT_SECRET_KEY;

import dotenv from 'dotenv';
dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

describe("User", () => {

	describe("Register", () => {
		describe("Given the user is a classic user", () => {
			it("should fail if credentials are missing", async () => {
				let user: any = {
					role: "user",
					firstname: "John",
					lastname: "Doe",
					email: "john@doe.com",
				};

				await supertest(app)
					.post("/api/user/register")
					.send(user)
					.expect(400)
					.expect({ message: "Missing parameters" });

				user = {
					role: "user",
					firstname: "John",
					lastname: "Doe",
					password: "password",
				};

				await supertest(app)
					.post("/api/user/register")
					.send(user)
					.expect(400)
					.expect({ message: "Missing parameters" });

				user = {
					role: "user",
					firstname: "John",
					email: "john@doe.com",
					password: "password",
				};

				await supertest(app)
					.post("/api/user/register")
					.send(user)
					.expect(400)
					.expect({ message: "Missing parameters" });

				user = {
					role: "user",
					lastname: "Doe",
					email: "john@doe.com",
					password: "password",
				};

				await supertest(app)
					.post("/api/user/register")
					.send(user)
					.expect(400)
					.expect({ message: "Missing parameters" });
			});

			it("should fail if email is invalid", async () => {
				let user: any = {
					role: "user",
					firstname: "John",
					lastname: "Doe",
					email: "johndoe.com",
					password: "password",
				};
				await supertest(app)
					.post("/api/user/register")
					.send(user)
					.expect(400)
					.expect({ message: "Invalid email" });
			});

			it("should fail if role is invalid", async () => {
				let user: any = {
					role: "admin",
					firstname: "John",
					lastname: "Doe",
					email: "john@doe.com",
					password: "password",
				};
				await supertest(app)
					.post("/api/user/register")
					.send(user)
					.expect(400)
					.expect({ message: "Invalid role" });
			});

			it("should fail if user already exists", async () => {
				// create a test user
				const testUser = {
					role: "user",
					firstname: "John",
					lastname: "Doe",
					email: "john@doe.com",
					password: "password",
				};

				// insert test user into database
				await prisma.user.create({ data: testUser });

				// attempt to register the same user again
				await supertest(app)
					.post("/api/user/register")
					.send(testUser)
					.expect(400)
					.expect({ message: "User already exists" });

				// delete

				await prisma.user.delete({
					where: {
						email: testUser.email,
					},
				});
			});

			it("should register a new user", async () => {
				// register new user
				const newUser = {
					role: "user",
					firstname: "Jane",
					lastname: "Doe",
					email: "jane@doe.com",
					password: "password",
				};

				await supertest(app)
					.post("/api/user/register")
					.send(newUser)
					.expect(201)
					.expect((res) => {
						const { message, data } = res.body;
						expect(message).toBe("User created");
						expect(data).toBeDefined();
					});

				// delete new user
				await prisma.user.delete({
					where: {
						email: newUser.email,
					},
				});
			});
		});

		describe("Given the user is a botanist", () => {
			it("should fail if role is botanist but there's no SIRET", async () => {
				const user = {
					role: "botanist",
					firstname: "Jane",
					lastname: "Doe",
					email: "jane@doe.com",
					password: "password",
				};

				await supertest(app)
					.post("/api/user/register")
					.send(user)
					.expect(400)
					.expect({ message: "Invalid role" });
			});

			it("should register a new botanist", async () => {
				// Write the test

				const newBotanist = {
					role: "botanist",
					firstname: "Jane",
					lastname: "Doe",
					email: "jane@doe.com",
					password: "password",
					siret: "12345678901234",
				};

				await supertest(app)
					.post("/api/user/register")
					.send(newBotanist)
					.expect(201)
					.expect((res) => {
						const { message, data } = res.body;
						expect(message).toBe("User created");
						expect(data).toBeDefined();
					});

				// delete new botanist
				await prisma.user.delete({
					where: {
						email: newBotanist.email,
					},
				});
			});
		});
	});

	describe("Delete", () => {

		let user;
		let userId: string;
		let token;

		beforeAll(async () => {
			// create a test user
			const testUser = {
				role: "user",
				firstname: "John",
				lastname: "Doe",
				email: "testdelete@test.com",
				password: "password",
			}

			// insert test user into database
			user = await prisma.user.create({ data: testUser });
			userId = user.id;

			token = jwt.sign(
				{
					sub: user!.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
			);

		})

		it("should delete a user", async () => {

			await supertest(app)
				.get(`/api/user/delete/${user!.id}`)
				.set("Cookie", [`jwt=${token!}`])
				.expect(200);

		});

		afterAll(async () => {

			try{
				// delete test user
			const user = await prisma.user.delete({
				where: {
					id:userId,
				}
			})
			}catch(error){
				console.log(error)
			}

		}, 10000)
	});

	describe("Ban", () => {

		let botanist;
		let admin;
		let botanistToken;
		let adminToken;

		beforeEach(async () => {
			botanist = await prisma.user.create({
				data: {
					role: "botanist",
					firstname: "John",
					lastname: "Doe",
					email: "botanist@botanist.com",
					password: "password",
					siret: "12345678901234",
					status: "pending",
				}
			})

			botanistToken = jwt.sign(
				{
					sub: botanist!.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
			)

			admin = await prisma.user.create({
				data: {
					role: "admin",
					firstname: "John",
					lastname: "Doe",
					email: "admin@botanist.com",
					password: "password",
				}
			})

			adminToken = jwt.sign(
				{
					sub: admin!.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
			)

		})

		afterEach(async () => {

			await prisma.user.deleteMany({
				where: {
					email: {
						in: ["botanist@botanist.com", "admin@botanist.com"]
					}
				}
			})

		})

		describe("Given admin want to ban an user", () => {
			
			it("should fail if req user is not admin", async () => {
				
				await supertest(app)
					.post(`/api/user/ban`)
					.send({id: botanist!.id})
					.set("Cookie", [`jwt=${botanistToken!}`])
					.expect(401)
					.expect({ message: "Unauthorized" })
			})

			it("should fail if user is not found", async () => {
				
				await supertest(app)
					.post(`/api/user/ban`)
					.send({id: "123456789012345678901234"})
					.set("Cookie", [`jwt=${adminToken!}`])
					.expect(404)
					.expect({ message: "User doesn't exist" })
			})

			it("should ban an user", async () => {
				
				await supertest(app)
					.post(`/api/user/ban`)
					.send({id: botanist!.id})
					.set("Cookie", [`jwt=${adminToken!}`])
					.expect(200)
					.expect({ message: "User banned" })
			})

		})

	})

	describe("Botanist register", () => {

		let botanist;
		let admin;
		let botanistToken;
		let adminToken;

		beforeEach(async () => {
			botanist = await prisma.user.create({
				data: {
					role: "botanist",
					firstname: "John",
					lastname: "Doe",
					email: "botanist@botanist.com",
					password: "password",
					siret: "12345678901234",
					status: "pending",
				}
			})

			botanistToken = jwt.sign(
				{
					sub: botanist!.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
			)

			admin = await prisma.user.create({
				data: {
					role: "admin",
					firstname: "John",
					lastname: "Doe",
					email: "admin@botanist.com",
					password: "password",
				}
			})

			adminToken = jwt.sign(
				{
					sub: admin!.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
			)

		})

		afterEach(async () => {

			await prisma.user.deleteMany({
				where: {
					email: {
						in: ["botanist@botanist.com", "admin@botanist.com"]
					}
				}
			})

		})

		describe("Given admin want to grand a botanist", () => {
			it("should fail if req user is not admin", async () => {
				
				await supertest(app)
					.post(`/api/user/botanist/grant`)
					.send({id: botanist!.id})
					.set("Cookie", [`jwt=${botanistToken!}`])
					.expect(401)
					.expect({ message: "Unauthorized" })
			})

			it("should fail if user is not found", async () => {
				
				await supertest(app)
					.post(`/api/user/botanist/grant`)
					.send({id: "123456789012345678901234"})
					.set("Cookie", [`jwt=${adminToken!}`])
					.expect(404)
					.expect({ message: "User doesn't exist" })
			})

			it("should fail if user is not a botanist", async () => {
				
				await supertest(app)
					.post(`/api/user/botanist/grant`)
					.send({id: admin!.id})
					.set("Cookie", [`jwt=${adminToken!}`])
					.expect(400)
					.expect({ message: "Invalid parameters" })
			})

			it("should grant a botanist", async () => {
				
				await supertest(app)
					.post(`/api/user/botanist/grant`)
					.send({id: botanist!.id})
					.set("Cookie", [`jwt=${adminToken!}`])
					.expect(200)
					.expect({ message: "User accepted" })
			})
		})

		describe("Given admin want to reject a botanist", () => {

			it("should fail if req user is not admin", async () => {
				
				await supertest(app)
					.post(`/api/user/botanist/reject`)
					.send({id: botanist!.id})
					.set("Cookie", [`jwt=${botanistToken!}`])
					.expect(401)
					.expect({ message: "Unauthorized" })
			})

			it("should fail if user is not found", async () => {
				
				await supertest(app)
					.post(`/api/user/botanist/reject`)
					.send({id: "123456789012345678901234"})
					.set("Cookie", [`jwt=${adminToken!}`])
					.expect(404)
					.expect({ message: "User doesn't exist" })
			})

			it("should fail if user is not a botanist", async () => {
				
				await supertest(app)
					.post(`/api/user/botanist/reject`)
					.send({id: admin!.id})
					.set("Cookie", [`jwt=${adminToken!}`])
					.expect(400)
					.expect({ message: "Invalid parameters" })
			})

			it("should reject a botanist", async () => {
				
				await supertest(app)
					.post(`/api/user/botanist/reject`)
					.send({id: botanist!.id})
					.set("Cookie", [`jwt=${adminToken!}`])
					.expect(200)
					.expect({ message: "User rejected" })
			})

		})

	})
});

