import supertest from "supertest";
import app from "../app";
import { PrismaClient } from "@prisma/client";
import * as fs from "fs";
import { convertDateToISO } from "../utils/date";
import axios from "axios";
const jwt = require("jsonwebtoken");
const jwtSecretKey = process.env.JWT_SECRET_KEY;
const prisma = new PrismaClient();

import dotenv from 'dotenv';
import path from 'path';
dotenv.config({ path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`)});

describe("Guard", () => {
	describe("Given we are creating a guard", () => {

    let user;
    let userToken;
    let guard;

    beforeEach(async () => {
    
      guard = undefined;

      user = await prisma.user.create({
        data: {
          role: "user",
          firstname: "John",
          lastname: "Doe",
          email: "create@guard.com",
          password: "test",
        }
      })

      userToken = jwt.sign(
        {
          sub: user.id,
          exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
        },
        jwtSecretKey
      )

    })

    afterEach(async () => {

      if(guard! && guard.id){
        
        const imageName = guard.plants[0].image;
  
        if(fs.existsSync(`public/uploads/${imageName!}`)) {
          fs.unlinkSync(`public/uploads/${imageName!}`)
        }

        await prisma.plant.delete({
          where: {
            id: guard.plants[0].id
          }
        })

        await prisma.guard.delete({
          where: {
            id: guard.id
          }
        })

      }

      await prisma.user.delete({
        where: {
          id: user!.id
        }
      })

    })

		it("should fail if user is not authenticated", async () => {
			const guard = {
				name: "test",
				description: "test",
				price: 10,
				stock: 10,
				plantImage: "test",
			};

			await supertest(app)
				.post("/api/guard/add")
				.send(guard)
				.expect(401)
				.expect({ message: "Unauthorized" });
		});

		it("should fail if data missing", async () => {

			const guard = {
				name: "test",
				description: "test",
				price: 10,
				stock: 10,
			};

			await supertest(app)
				.post("/api/guard/add")
				.set("Cookie", [`jwt=${userToken!}`])
				.send(guard)
				.expect(400)
				.expect({ message: "Missing parameters" });

		});

		it("should update a guard", async () => {
				
				const guard = await prisma.guard.create({
          data: {
            owner: {
              connect: {
                id: user!.id
              }
            },
            startDate: new Date(),
            endDate: new Date(),
            address: "125 rue des Arbres",
            zipCode: "34000",
            city: "Montpellier"
          }
        })
				
				const updatedGuard = {
					startDate: "12-01-2024",
					endDate: "22-01-2024",
					address: "125 rue des Arbres",
					zipCode: "75000",
					city: "Paris"
				}

				await supertest(app)
					.post(`/api/guard/edit/${guard?.id}`)
					.set("Cookie", [`jwt=${userToken!}`])
					.send(updatedGuard)
					.expect(200)
					.expect((res) => {
						const { message, data } = res.body;
						expect(message).toBe("Guard updated");
						expect(data).toBeDefined();
					});

				await prisma.guard.delete({
					where:{
						id: guard?.id
					}
				})

		})
	});

	describe("Given we are getting available guards", () => {

		it("should fail if user is not authenticated", async () => {

			const data = {
				filters: {
					city:'',
					plantTypes:''
				}
			}

			await supertest(app)
				.post("/api/guard/availables")
				.send(data)
				.expect(401)
				.expect({ message: "Unauthorized" });
		});

		it("should get available guards", async () => {

      const normalUser = {
				role: "user",
				firstname: "John",
				lastname: "Doe",
				password: "password",
				email: "test@guardavailable.com"
			}

      const botanistUser = {
        role: "botanist",
        firstname: "John",
        lastname: "Doe",
        password: "password",
        email: "test2@guardavailable.com"
      }

      const normalUserCreated = await prisma.user.create({
        data: normalUser
      })

      const botanistUserCreated = await prisma.user.create({
        data: botanistUser
      })

      const normalUserToken = jwt.sign(
        {
					sub: normalUserCreated.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
      );

      const botanistUserToken = jwt.sign(
        {
          sub: botanistUserCreated.id,
          exp: Date.now() + 1000 * 60 * 60 * 24 * 7
        },
        jwtSecretKey
      )

      // Get current date, and export three periods with format DD-MM-YYYY (fr)
      // Each period is 2 dates (start and end) with 10 days between them
      // On period is in the past, two are in the future
      
      const today = new Date();
      const twentyDays = 1000 * 60 * 60 * 24 * 20;
      const tenDays = 1000 * 60 * 60 * 24 * 10;

      const period1Start = new Date(today.getTime() - twentyDays).toLocaleDateString('fr-FR').replaceAll('/','-');
      const period1End = new Date(today.getTime() - tenDays).toLocaleDateString('fr-FR').replaceAll('/','-');

      const period2Start = today.toLocaleDateString('fr-FR').replaceAll('/','-');
      const period2End = new Date(today.getTime() + tenDays).toLocaleDateString('fr-FR').replaceAll('/','-');

      const period1StartISO = convertDateToISO(period1Start);
      const period1EndISO = convertDateToISO(period1End);
      const period2StartISO = convertDateToISO(period2Start);
      const period2EndISO = convertDateToISO(period2End);

      // Add guards in db corresponding to each period
      // For the two periods ine future, one don't have guardian, and other have one (user id)
      // Guards don't have plant

      const guard1 = await prisma.guard.create({
        data: {
          ownerId: normalUserCreated.id,
          startDate: period1StartISO,
          endDate: period1EndISO,
          address: "125 rue des Arbres",
          zipCode: "34000", 
          city: "Montpellier"
        }
      })

      const guard2 = await prisma.guard.create({
        data: {
          ownerId: normalUserCreated.id,
          startDate: period2StartISO,
          endDate: period2EndISO,
          address: "125 rue des Arbres",
          zipCode: "34000",
          city: "Montpellier"  
        }
      })

      const guard3 = await prisma.guard.create({
        data: {
          ownerId: normalUserCreated.id,
          startDate: period2StartISO,
          endDate: period2EndISO,
          address: "125 rue des Arbres",
          zipCode: "34000",
          city: "Montpellier",
          guardianId: botanistUserCreated.id
        }
      })

      // test the route, expect to have only one guard in response

      const data = {
        filters: {
          city:'',
          plantTypes:''
        }
      }

      await supertest(app)
        .post("/api/guard/availables")
        .set("Cookie", [`jwt=${normalUserToken}`]) 
        .send(data)
        .expect(200)
        .expect(res => {
          expect(res.body.data.guards.length).toEqual(1);
        });

      // delete guards
      await prisma.guard.deleteMany({
        where: {
          id: {
            in: [guard1.id, guard2.id, guard3.id]
          }
        }
      })

      // Delete users
      
      await prisma.user.deleteMany({
        where: {
          email: {
            in: ["test@guardavailable.com", "test2@guardavailable.com"]
          }
        }
      })


		})

	})

  describe("Given we are getting user guards", () => {

    it("should get logged user guards", async () => {

      const normalUser = {
				role: "user",
				firstname: "John",
				lastname: "Doe",
				password: "password",
				email: "test@loggeduserguards.com"
			}

      const normalUserCreated = await prisma.user.create({
        data: normalUser
      })

      const normalUserToken = jwt.sign(
        {
					sub: normalUserCreated.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
      );

      // Get current date, and export three periods with format DD-MM-YYYY (fr)
      // Each period is 2 dates (start and end) with 10 days between them
      // On period is in the past, two are in the future
      
      const today = new Date();
      const tenDays = 1000 * 60 * 60 * 24 * 10;

      const period1Start = new Date(today.getTime()).toLocaleDateString('fr-FR').replaceAll('/','-');
      const period1End = new Date(today.getTime() + tenDays).toLocaleDateString('fr-FR').replaceAll('/','-');

      const period1StartISO = convertDateToISO(period1Start);
      const period1EndISO = convertDateToISO(period1End);

      // Add guards in db corresponding to each period
      // For the two periods ine future, one don't have guardian, and other have one (user id)
      // Guards don't have plant

      const guard1 = await prisma.guard.create({
        data: {
          ownerId: normalUserCreated.id,
          startDate: period1StartISO,
          endDate: period1EndISO,
          address: "125 rue des Arbres",
          zipCode: "34000", 
          city: "Montpellier"
        }
      })

      // test the route, expect to have only one guard in response

      await supertest(app)
        .get("/api/guard/userGuards")
        .set("Cookie", [`jwt=${normalUserToken}`]) 
        .expect(200)

      // delete guards
      await prisma.guard.deleteMany({
        where: {
          id: {
            in: [guard1.id]
          }
        }
      })

      // Delete users
      
      await prisma.user.deleteMany({
        where: {
          email: {
            in: ["test@loggeduserguards.com"]
          }
        }
      })


		})

    it("should get guards by user id", async () => {

      const normalUser = {
				role: "user",
				firstname: "John",
				lastname: "Doe",
				password: "password",
				email: "test@guardsbyuserid.com"
			}

      const normalUserCreated = await prisma.user.create({
        data: normalUser
      })

      const normalUserToken = jwt.sign(
        {
					sub: normalUserCreated.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
      );

      // Get current date, and export three periods with format DD-MM-YYYY (fr)
      // Each period is 2 dates (start and end) with 10 days between them
      // On period is in the past, two are in the future
      
      const today = new Date();
      const tenDays = 1000 * 60 * 60 * 24 * 10;

      const period1Start = new Date(today.getTime()).toLocaleDateString('fr-FR').replaceAll('/','-');
      const period1End = new Date(today.getTime() + tenDays).toLocaleDateString('fr-FR').replaceAll('/','-');

      const period1StartISO = convertDateToISO(period1Start);
      const period1EndISO = convertDateToISO(period1End);

      // Add guards in db corresponding to each period
      // For the two periods ine future, one don't have guardian, and other have one (user id)
      // Guards don't have plant

      const guard1 = await prisma.guard.create({
        data: {
          ownerId: normalUserCreated.id,
          startDate: period1StartISO,
          endDate: period1EndISO,
          address: "125 rue des Arbres",
          zipCode: "34000", 
          city: "Montpellier"
        }
      })

      // test the route, expect to have only one guard in response

      await supertest(app)
        .get("/api/guard/user/"+normalUserCreated.id)
        .set("Cookie", [`jwt=${normalUserToken}`]) 
        .expect(200)

      // delete guards
      await prisma.guard.deleteMany({
        where: {
          id: {
            in: [guard1.id]
          }
        }
      })

      // Delete users
      
      await prisma.user.deleteMany({
        where: {
          email: {
            in: ["test@guardsbyuserid.com"]
          }
        }
      })


		})

  })

  describe("Given we manage applies to guards", () => {

    let guard1;
    let user1;
    let user2;
    let user1Token;
    let user2Token;


    beforeAll(async () => {
      const user1Data = {
        role: "user",
        firstname: "John",
        lastname: "Doe",
        password: "password",
        email: "user1@applies.com"
      }
      const user2Data = {
        role: "user",
        firstname: "John",
        lastname: "Doe",
        password: "password",
        email: "user2@applies.com"
      }

      user1 = await prisma.user.create({
        data: user1Data
      })

      user2 = await prisma.user.create({
        data: user2Data
      })

      user1Token = jwt.sign(
        {
					sub: user1.id,
					exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
				},
				jwtSecretKey
      );

      user2Token = jwt.sign(
        {
          sub: user2.id,
          exp: Date.now() + 1000 * 60 * 60 * 24 * 7, // 1 week
        },
        jwtSecretKey
      );

      const today = new Date();
      const tenDays = 1000 * 60 * 60 * 24 * 10;

      const guardData = {
        startDate: convertDateToISO(new Date(today.getTime()).toLocaleDateString('fr-FR').replaceAll('/','-')),
        endDate: convertDateToISO(new Date(today.getTime() + tenDays).toLocaleDateString('fr-FR').replaceAll('/','-')),
        address: "125 rue des Arbres",
        zipCode: "34000", 
        city: "Montpellier"
      }

      guard1 = await prisma.guard.create({
        data: {
          ...guardData,
          ownerId: user1.id
        }
      })

    })

    afterAll(async () => {
      await prisma.guard.deleteMany({
        where: {
          id: {
            in: [guard1!.id]
          }
        }
      })

      await prisma.user.deleteMany({
        where: {
          email: {
            in: ["user1@applies.com","user2@applies.com"]
          }
        }
      })
    })

    it("should add an apply to a guard", async () => {
      
      await supertest(app)
        .get("/api/guard/apply/"+guard1!.id)
        .set("Cookie", [`jwt=${user2Token!}`]) 
        .expect(200)
        .expect(res => {
          expect(res.body.message).toEqual("Application created");
        });

      const application = await prisma.application.findFirst({
        where: {
          guardId: guard1!.id,
          userId: user2!.id
        }
      })

      expect(application).toBeDefined();

      await prisma.application.delete({
        where: {
          id: application!.id
        }
      })

    })

    it("should remove an apply to a guard", async () => {

      const application = await prisma.application.create({
        data: {
          guardId: guard1!.id,
          userId: user2!.id
        }
      })

      await supertest(app)
        .get("/api/guard/apply/remove/"+application!.id)
        .set("Cookie", [`jwt=${user2Token!}`]) 
        .expect(200)
        .expect(res => {
          expect(res.body.message).toEqual("Application deleted");
        });

      const applicationRemoved = await prisma.application.findFirst({
        where: {
          id: application!.id
        }
      })

      expect(applicationRemoved).toBeNull();

    })

    it("should confirm an apply to a guard", async () => {

      const application = await prisma.application.create({
        data: {
          guardId: guard1!.id,
          userId: user2!.id
        }
      })

      await supertest(app)
        .get("/api/guard/apply/confirm/"+application!.id)
        .set("Cookie", [`jwt=${user1Token!}`]) 
        .expect(200)
        .expect(res => {
          expect(res.body.message).toEqual("Application confirmed");
        });

      const guardId = guard1!.id

      guard1 = await prisma.guard.findFirst({
        where: {
          id: guardId
        }
      })

      expect(guard1!.guardianId).toEqual(user2!.id);
      
      await prisma.conversation.delete({
        where: {
          guardId: guard1!.id
        }
      })

    })

  })
});
