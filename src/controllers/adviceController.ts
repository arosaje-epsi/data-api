import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export const addGuardAdvice = async (req: any, res: any, next: any) => {

  if(req.user.role !== 'botanist') {
    return res.status(401).json({error: 'Unauthorized'})
  }

  const {guardId, content} = req.body

  if (!guardId || !content) {
    return res.status(400).json({error: 'Missing required fields'})
  }

  try {
    
    const guard = await prisma.guard.findUnique({where: {id: guardId}})

    if (!guard) {
      return res.status(404).json({error: 'Guard not found'})
    }

    const advice = await prisma.advice.create({
      data: {
        user: {
          connect: {
            id: req.user.id
          }
        },
        referer: 'guard',
        guard: {
          connect: {
            id: guardId
          }
        },
        content
      }
    })

    return res.status(201).json({message: 'Advice added', data: {advice}})    

  } catch (error) {
    console.log(error)
    return res.status(500).json({error: 'Internal server error'})
  }

}

export const addVisitAdvice = async (req: any, res: any, next: any) => {

  if(req.user.role !== 'botanist') {
    return res.status(401).json({error: 'Unauthorized'})
  }

  const {visitId, content} = req.body

  if (!visitId ||!content) {
    return res.status(400).json({error: 'Missing required fields'})
  }

  try {
    
    const visit = await prisma.visit.findUnique({where: {id: visitId}})

    if (!visit) {
      return res.status(404).json({error: 'Visit not found'})
    }

    const advice = await prisma.advice.create({
      data: {
        user: {
          connect: {
            id: req.user.id
          }
        },
        referer: 'visit',
        visit: {
          connect: {
            id: visitId
          }
        },
        content
      }
    })

    return res.status(201).json({message: 'Advice added', data: {advice}})    

  } catch (error) {
    console.log(error)
    return res.status(500).json({error: 'Internal server error'})
  }

}

export const getGuardAdvices = async (req: any, res: any, next: any) => {

  const {guardId} = req.params

  if (!guardId) {
    return res.status(400).json({error: 'Missing required fields'})
  }

  try {
    
    const guard = await prisma.guard.findUnique({where: {id: guardId}})

    if (!guard) {
      return res.status(404).json({error: 'Guard not found'})
    }

    const advices = await prisma.advice.findMany({
      where: {
        guardId
      },
      include: {
        user: true
      }
    })

    const message = advices.length > 0 ? 'Advices found' : 'No advices found'

    return res.status(200).json({message,data: {advices}})    

  } catch (error) {
    console.log(error)
    return res.status(500).json({error: 'Internal server error'})
  }

}

export const getVisitAdvices = async (req: any, res: any, next: any) => {

  const {visitId} = req.params

  if (!visitId) {
    return res.status(400).json({error: 'Missing required fields'})
  }

  try {
    
    const visit = await prisma.visit.findUnique({where: {id: visitId}})

    if (!visit) {
      return res.status(404).json({error: 'Visit not found'})
    }

    const advices = await prisma.advice.findMany({
      where: {
        visitId
      },
      include: {
        user: true
      }
    })

    const message = advices.length > 0 ? 'Advices found' : 'No advices found'

    return res.status(200).json({message,data: {advices}})    

  } catch (error) {
    console.log(error)
    return res.status(500).json({error: 'Internal server error'})
  }

}