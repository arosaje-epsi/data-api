import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export const getTypes = async (req: any, res: any, next: any) => {
  try {
    const types = await prisma.plantType.findMany();
    return res.status(200).json(types);
  } catch (error) {
    return res.status(400).json({ message: error });
  }
}