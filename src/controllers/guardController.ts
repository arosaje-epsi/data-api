import { PrismaClient } from "@prisma/client";
import { convertDateToISO } from "../utils/date";
import { count } from "console";
const prisma = new PrismaClient();
const path = require("path");

export const addGuard = async (req: any, res: any, next: any) => {

  const user = req.user
  const {startDate, endDate, address, zipCode, city} = req.body;
  const plantsData = []

  if(!req.files || !startDate || !endDate || !address || !zipCode || !city){
    return res.status(400).json({message: "Missing parameters"})
  }

  // Get plant data fields
  for(let i = 1; i <= req.files.plantImage.length; i++){

    // Check if data exist
    const plantName:string = req.body[`plantName${i}`]
    const plantType:string = req.body[`plantType${i}`]
    
    if(!plantName || !plantType){
      return res.status(400).json({message: "Missing parameters"})
    }

    // Check if plant type exist
    const checkPlantType = await prisma.plantType.findFirst({
      where: {
        name: plantType
      }    
    })

    if(!checkPlantType){
      return res.status(400).json({message: "Plant type not found"})
    }
    plantsData.push({
      image: req.files.plantImage[i-1],
      name: req.body[`plantName${i}`],	
      type: req.body[`plantType${i}`],
    })

  }

  // ###
  // DATA OK
  // ###
  // Create guard
  try {
    const guard = await prisma.guard.create({
      data: {
        startDate: convertDateToISO(startDate),
        endDate: convertDateToISO(endDate),
        address,
        zipCode,
        city,
        owner: {
          connect: {
            id: user.id
          }
        }
      }
    })

    // Create plants
    for(let i = 0; i < plantsData.length; i++){

      const plant = await prisma.plant.create({
        data: {
          name: plantsData[i].name,
          image: plantsData[i].image.filename,
          type: plantsData[i].type,
          guard: {
            connect: {
              id: guard.id
            }
          }
        }
      })


    }

    // Get returned guard

    const returnedGuard = await prisma.guard.findUnique({
      where: {
        id: guard.id
      },
      include: {
        plants: true,
        owner: {
          select: {
            id: true,
            email: true,
            firstname: true,
            lastname: true,
            role: true,
            avatar: true,
            createdAt: true,
          }
        }
      }
    })


    return res.status(201).json({message:"Guard created",data:{guard:returnedGuard}})
    
  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})
  }



}

export const updateGuard = async (req: any, res: any, next: any) => {

  const user = req.user
  const guardId = req.params.guardId

  if(!guardId){
    return res.status(400).json({message: "Missing guardId"})
  }

  const {startDate, endDate, address, zipCode, city} = req.body;

  if(!startDate || !endDate || !address || !zipCode || !city){
    return res.status(400).json({message: "Missing parameters"})
  }

  try {
    
    // Check if guard exist
    const guard = await prisma.guard.findUnique({
      where: {
        id: guardId
      }
    })

    if(!guard){
      return res.status(400).json({message: "Guard not found"})
    }

    // Check if user is the owner
    if(guard.ownerId !== user.id){
      return res.status(400).json({message: "You can't update this guard"})
    }

    // Update guard
    const updatedGuard = await prisma.guard.update({
      where: {
        id: guardId
      },
      data: {
        startDate: convertDateToISO(startDate),
        endDate: convertDateToISO(endDate),
        address,
        zipCode,
        city,
      }
    })

    return res.status(200).json({message:"Guard updated",data:{guard:updatedGuard}})

  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})
  }

}

export const getAllGuards = async (req: any, res: any, next: any) => {

  if(req.user.role !== 'admin'){
    return res.status(403).json({message: "Forbidden"})
  }
  
  try {

    const guards = await prisma.guard.findMany({
      include: {
        plants: true,
        owner: {
          select: {
            id: true,
            email: true,
            firstname: true,
            lastname: true,
            role: true,
            avatar: true,
            createdAt: true,
          }
        }
      }
    })

    return res.status(200).json({message:"Guards found",data:{guards}})

  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})
  }
  
}

export const avaiablesGuards = async (req: any, res: any, next: any) => {
  
  const filters = req.body.filters

  if(!filters){
    return res.status(400).json({message: "Missing filters object"})
  }

  const isBotanist = req.user.role === 'botanist'

  const today = new Date();
  today.setHours(0, 0, 0, 0);

  try{
    // Both filters
    if((filters.plantTypes && filters.plantTypes.length > 0) && (filters.city && filters.city !== '')){

      let guards;
      
      if(isBotanist){

        guards = await prisma.guard.findMany({
          where: {
            endDate: {
              gt: new Date()
            },
            plants: {
              some: {
                type: {
                  in: filters.plantTypes
                }
              }
            },
            city: filters.city,
          },
          include: {
            plants: true,
            owner: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                role: true,
                avatar: true,
                createdAt: true,
              }
            }
          }
        })

      }else{

        guards = await prisma.guard.findMany({
          where: {
            endDate: {
              gt: new Date()
            },
            plants: {
              some: {
                type: {
                  in: filters.plantTypes
                }
              }
            },
            city: filters.city,
            guardian: null
          },
          include: {
            plants: true,
            owner: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                role: true,
                avatar: true,
                createdAt: true,
              }
            }
          }
        })

      }

      const message = guards.length > 0 ? "Guards found" : "No guards found"

      return res.status(200).json({message,data:{guards}})
      
    }
    // Only plant type filter
    else if ((filters.plantTypes && filters.plantTypes.length > 0) && (!filters.city || filters.city === '')){

      let guards;

      if(isBotanist){

        guards = await prisma.guard.findMany({
          where: {
            endDate: {
              gt: new Date()
            },
            plants: {
              some: {
                type: {
                  in: filters.plantTypes
                }
              }
            },
          },
          include: {
            plants: true,
            owner: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                role: true,
                avatar: true,
                createdAt: true,
              }
            }
          }
        })

      }else{

        guards = await prisma.guard.findMany({
          where: {
            endDate: {
              gt: new Date()
            },
            plants: {
              some: {
                type: {
                  in: filters.plantTypes
                }
              }
            },
            guardian: null
          },
          include: {
            plants: true,
            owner: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                role: true,
                avatar: true,
                createdAt: true,
              }
            }
          }
        })

      }

      const message = guards.length > 0 ? "Guards found" : "No guards found"

      return res.status(200).json({message,data:{guards}})


    }
    // Only city filter
    else if ((!filters.plantTypes || filters.plantTypes.length === 0) && (filters.city && filters.city !== '')){

      let guards;

      if(isBotanist){

        guards = await prisma.guard.findMany({
          where: {
            endDate: {
              gt: new Date()
            },
            city: filters.city,
          },
          include: {
            plants: true,
            owner: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                role: true,
                avatar: true,
                createdAt: true,
              }
            }
          }
        })

      }else{

        guards = await prisma.guard.findMany({
          where: {
            endDate: {
              gt: new Date()
            },
            city: filters.city,
            guardian: null
          },
          include: {
            plants: true,
            owner: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                role: true,
                avatar: true,
                createdAt: true,
              }
            }
          }
        })

      }

      const message = guards.length > 0 ? "Guards found" : "No guards found"

      return res.status(200).json({message,data:{guards}})

    }
    // No filters
    else{

      let guards;

      if(isBotanist){
        guards = await prisma.guard.findMany({
          where: {
            endDate: {
              gte: today
            }
          },
          include: {
            plants: true,
            owner: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                role: true,
                avatar: true,
                createdAt: true,
              }
            }
          }
        })
      }else{
        guards = await prisma.guard.findMany({
          where: {
            guardianId: null,
            endDate: {
              gte: today
            }
          },
          include: {
            plants: true,
            owner: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                role: true,
                avatar: true,
                createdAt: true,
              }
            }
          }
        })
      }

      const message = guards.length > 0 ? "Guards found" : "No guards found"

      return res.status(200).json({message,data:{guards}})

    }

  } catch (err) {
    console.log(err)
    return res.status(500).json({message: "Internal server error"})
  }

}

export const userGuards = async (req: any, res: any, next: any) => {

  const user = req.user

  const guardsRequested = []
  const guardsMade = []

  try {

    // Get owned guards
    const guards = await prisma.guard.findMany({
      where: {
        ownerId: user.id
      },
      include: {
        plants: true,
        owner: {
          select: {
            id: true,
            email: true,
            firstname: true,
            lastname: true,
            role: true,
            avatar: true,
            createdAt: true,
          }
        }
      }
    })

    for(let i = 0; i < guards.length; i++){
      guardsRequested.push(guards[i])
    }

    // Get guarded guards
    const guarded = await prisma.guard.findMany({
      where: {
        guardianId: user.id
      },
      include: {
        plants: true,
        owner: {
          select: {
            id: true,
            email: true,
            firstname: true,
            lastname: true,
            role: true,
            avatar: true,
            createdAt: true,
          }
        }
      }
    })

    for(let i = 0; i < guarded.length; i++){
      guardsMade.push(guarded[i])
    }

    const message = guardsMade.length > 0 || guardsRequested.length > 0 ? "Guards found" : "No guards found"

    return res.status(200).json({message: message, data: {guardsRequested, guardsMade}})

  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})
  }
}

export const getGuardsByUserId = async (req: any, res: any, next: any) => {

  const userId = req.params.userId

  if(!userId){
    return res.status(400).json({message: "Missing userId"})
  }

  try {

    // Get owned guards
    const guardsRequested = await prisma.guard.findMany({
      where: {
        ownerId: userId
      },
      include: {
        plants: true,
        owner: {
          select: {
            id: true,
            email: true,
            firstname: true,
            lastname: true,
            role: true,
            avatar: true,
            createdAt: true,
          }
        }
      }
    })

    // Get guarded guards
    const guardsMade = await prisma.guard.findMany({
      where: {
        guardianId: userId
      },
      include: {
        plants: true,
        owner: {
          select: {
            id: true,
            email: true,
            firstname: true,
            lastname: true,
            role: true,
            avatar: true,
            createdAt: true,
          }
        }
      }
    })

    const message = guardsMade.length > 0 || guardsRequested.length > 0 ? "Guards found" : "No guards found"

    return res.status(200).json({message: message, data: {guardsRequested, guardsMade}})

  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})
  }

}

export const addApply = async (req: any, res: any, next: any) => {

  const guardId = req.params.guardId
  const userId = req.user.id

  if(!guardId || !userId){
    return res.status(400).json({message: "Missing parameters"})
  }

  try {

    // Check if guard exist and user not already in applicants[user], and user is not owner
    const guard = await prisma.guard.findUnique({
      where: {
        id: guardId
      }
    })

    if(!guard){
      return res.status(400).json({message: "Guard not found"})
    }

    if(guard.ownerId === userId){
      return res.status(400).json({message: "You can't apply to your own guard"})
    }

    // Check if application exist
    const application = await prisma.application.findFirst({
      where: {
        guardId: guardId,
        userId: userId
      }
    })

    if(application){
      return res.status(400).json({message: "You already applied to this guard"})
    }

    // Create application
    const newApplication = await prisma.application.create({
      data: {
        guard: {
          connect: {
            id: guardId
          }
        },
        user: {
          connect: {
            id: userId
          }
        }
      }
    })

    return res.status(200).json({message: "Application created", data: {application: newApplication}})

  } catch (error) {
    
  }

}

export const removeApply = async (req: any, res: any, next: any) => {

  const applicationId = req.params.applicationId
  const userId = req.user.id

  if(!applicationId || !userId){
    return res.status(400).json({message: "Missing parameters"})
  }

  try {

    // Check if application exist
    const application = await prisma.application.findFirst({
      where: {
        OR: [
          {
            id: applicationId,
            userId: userId
          },
          {
            id: applicationId,
            guard: {
              ownerId: userId
            }
          }
        ]
      }
    })



    if(!application){
      return res.status(400).json({message: "Application not found"})
    }

    // Delete application
    await prisma.application.delete({
      where: {
        id: applicationId
      }
    })

    return res.status(200).json({message: "Application deleted"})

  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})
  }

}

export const confirmApply = async (req: any, res: any, next: any) => {
  
    const applicationId = req.params.applicationId
    const userId = req.user.id
  
    if(!applicationId || !userId){
      return res.status(400).json({message: "Missing parameters"})
    }
  
    try {
  
      // Check if application exist
      const application = await prisma.application.findFirst({
        where: {
          id: applicationId,
          guard: {
            ownerId: userId
          }
        }
      })
  
      if(!application){
        return res.status(400).json({message: "Application not found"})
      }

      // check if guard exist and logged user is the owner
      const guard = await prisma.guard.findUnique({
        where: {
          id: application.guardId
        }
      })

      if(!guard){
        return res.status(400).json({message: "Guard not found"})
      }

      if(guard.ownerId !== userId){
        return res.status(400).json({message: "You can't confirm this application"})
      }
  
      // Update guard
      const updatedGuard = await prisma.guard.update({
        where: {
          id: application.guardId
        },
        data: {
          guardian: {
            connect: {
              id: application.userId
            }
          }
        }
      })
  
      // Delete all applications
      await prisma.application.deleteMany({
        where: {
          guardId: application.guardId
        }
      })

      // Create related conversation
      const newConversation = await prisma.conversation.create({
        data: {
          guard: {
            connect: {
              id: application.guardId
            }
          },
        }
      })
  
      return res.status(200).json({message: "Application confirmed", data: {guard: updatedGuard}})
  
    } catch (error) {
      console.log(error)
      return res.status(500).json({message: "Internal server error"})
    }
}

export const getGuardDetails = async (req: any, res: any, next: any) => {

  const guardId = req.params.guardId

  if(!guardId){
    return res.status(400).json({message: "Missing guardId"})
  }

  try {

    // Get guard
    const guard = await prisma.guard.findUnique({
      where: {
        id: guardId
      },
      include: {
        applications: {
          include: {
            user: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                role: true,
                avatar: true,
                createdAt: true,
                _count: {
                  select: {
                    guardsMade: true
                  }
                }
              }
            },
          }
        },
        plants: true,
        visits: true,
        advice: true,
        conversation: true,
        owner: {
          select: {
            id: true,
            email: true,
            firstname: true,
            lastname: true,
            role: true,
            avatar: true,
            createdAt: true,
          }
        },
        guardian: {
          select: {
            id: true,
            email: true,
            firstname: true,
            lastname: true,
            role: true,
            avatar: true,
            createdAt: true,
          }
        }
      }
    })

    if(!guard){
      return res.status(400).json({message: "Guard not found"})
    }

    return res.status(200).json({message: "Guard found", data: {guard}})

  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})
  }

}