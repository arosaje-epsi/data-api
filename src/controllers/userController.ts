import { PrismaClient } from "@prisma/client";
import { create } from "domain";
const bcrypt = require("bcrypt");
const prisma = new PrismaClient();

export const userCreate = async (req: any, res: any, next: any) => {
	try {
		const { role, firstname, lastname, email, password, siret } = req.body;

		// Check if credentials exist
		if (!role || !firstname || !lastname || !email || !password) {
			return res.status(400).json({ message: "Missing parameters" });
		}

		// Check if email is valid
		if (!email.includes("@") || !email.includes(".")) {
			return res.status(400).json({ message: "Invalid email" });
		}

		// Check if role == "user" | "botanist"
		if (role !== "user" && !(role === "botanist" && siret)) {
			return res.status(400).json({ message: "Invalid role" });
		}    

		// Check if user already exists
		const existingUser = await prisma.user.findUnique({
			where: {
				email: email,
			},
		});

		if (existingUser) {
			return res.status(400).json({ message: "User already exists" });
		}

		// Create user
		// Hash password
		const hashedPassword = await bcrypt.hash(password, 10);

		// Create user
    let user = undefined;

		if(role === "user") {
      user = await prisma.user.create({
        data: {
          role: role,
          firstname: firstname,
          lastname: lastname,
          email: email,
          password: hashedPassword,
        },
      });
    } else if(role === "botanist") {
      user = await prisma.user.create({
        data: {
          role: role,
          firstname: firstname,
          lastname: lastname,
          email: email,
          password: hashedPassword,
          siret: siret,
          status: "pending",
        },
      });
    }

    if(user){
      const {password, ...returnedUser} = user
      return res.status(201).json({ message: "User created", data:{ user:{...returnedUser} }});
    }else{
      return res.status(500).json({ message: "Internal Server Error" });
    }


	} catch (error) {
		return res.status(500).json({ message: "Internal Server Error" });
	}
};

export const userDelete = async (req: any, res: any, next: any) => {

	try {
		const { id } = req.params;

		// Check if user exists
		const existingUser = await prisma.user.findUnique({
			where: {
				id: id,
			},
		});

		if (!existingUser) {
			return res.status(400).json({ message: "User doesn't exist" });
		}

		// Check if user is the same as the one in the token
		if(existingUser.id !== req.user.id || (existingUser.id !== req.user.id && req.user.role !== "admin")){
			return res.status(401).json({ message: "Unauthorized" });
		}

		// Soft delete the user
		const deletedUser = await prisma.user.update({
			where: {
				id: id,
			},
			data: {
				email: "deleted",
				firstname: "deleted",
				lastname: "deleted",
				password: "deleted",
				siret: "deleted",
				avatar: "https://i.imgur.com/6VBx3io.png",
				status: "deleted",
			},
		})

		const deletedGuards = await prisma.guard.updateMany({
			where: {
				ownerId: id,
			},
			data: {
				address: "deleted",
				zipCode: "deleted",
				city: "deleted",
			},
		})

		if(req.user.role === "admin"){
			return res.status(200).json({ message: "User deleted" });
		}else{
			return res.status(200).clearCookie("jwt").json({ message: "User deleted" });
		}

	} catch (error) {
		return res.status(500).json({ message: error });
	}

}

export const userBan = async (req: any, res: any, next: any) => {

	// Check if req user is admin
	if(req.user.role !== "admin"){
		return res.status(401).json({ message: "Unauthorized" });
	}

	try {
		const { id } = req.body;

		// Check if user exists
		const existingUser = await prisma.user.findUnique({
			where: {
				id: id,
			},
		});

		if (!existingUser) {
			return res.status(404).json({ message: "User doesn't exist" });
		}

		// Ban the user
		const bannedUser = await prisma.user.update({
			where: {
				id: id,
			},
			data: {
				status: "rejected",
			},
		})

		return res.status(200).json({ message: "User banned" });

	} catch (error) {
		return res.status(500).json({ message: error });
	}

}

export const userGrant = async (req: any, res: any, next: any) => {

	// Check if req user is admin
	if(req.user.role !== "admin"){
		return res.status(401).json({ message: "Unauthorized" });
	}

	try {
		const { id } = req.body;

		// Check if user exists
		const existingUser = await prisma.user.findUnique({
			where: {
				id: id,
			},
		});

		if (!existingUser) {
			return res.status(404).json({ message: "User doesn't exist" });
		}

		// Grant user
		const grantedUser = await prisma.user.update({
			where: {
				id: id,
			},
			data: {
				status: "accepted",
			},
		})

		return res.status(200).json({ message: "User granted" });

	} catch (error) {
		return res.status(500).json({ message: error });
	}

}

export const userPending = async (req: any, res: any, next: any) => {
	
	// Check if req user is admin
	if(req.user.role !== "admin"){
		return res.status(401).json({ message: "Unauthorized" });
	}

	try {
		const { id } = req.body;

		// Check if user exists
		const existingUser = await prisma.user.findUnique({
			where: {
				id: id,
			},
		});

		if (!existingUser) {
			return res.status(404).json({ message: "User doesn't exist" });
		}

		// Set user to pending
		const pendingUser = await prisma.user.update({
			where: {
				id: id,
			},
			data: {
				status: "pending",
			},
		})

		return res.status(200).json({ message: "User set to pending" });

	} catch (error) {
		return res.status(500).json({ message: error });
	}


}

export const setBotanistAccepted = async (req: any, res: any, next: any) => {

	// Check if rq user is admin
	if(req.user.role !== "admin"){
		return res.status(401).json({ message: "Unauthorized" });
	}

	try {
		const { id } = req.body;

		// Check if user exists
		const existingUser = await prisma.user.findUnique({
			where: {
				id: id,
			},
		});

		if (!existingUser) {
			return res.status(404).json({ message: "User doesn't exist" });
		}

		if(existingUser.status !== "pending" || existingUser.role !== "botanist"){
			return res.status(400).json({ message: "Invalid parameters" });
		}

		// Grant user
		const acceptedBotanist = await prisma.user.update({
			where: {
				id: id,
			},
			data: {
				status: "accepted",
			},
		})

		return res.status(200).json({ message: "User accepted" });

	} catch (error) {
		return res.status(500).json({ message: error });
	}

}

export const setBotanistRejected = async (req: any, res: any, next: any) => {

	// Check if rq user is admin
	if(req.user.role !== "admin"){
		return res.status(401).json({ message: "Unauthorized" });
	}

	try {
		const { id } = req.body;

		// Check if user exists
		const existingUser = await prisma.user.findUnique({
			where: {
				id: id,
			},
		});

		if (!existingUser) {
			return res.status(404).json({ message: "User doesn't exist" });
		}

		if(existingUser.status !== "pending" || existingUser.role !== "botanist"){
			return res.status(400).json({ message: "Invalid parameters" });
		}

		// Reject user
		const rejectedBotanist = await prisma.user.update({
			where: {
				id: id,
			},
			data: {
				status: "rejected",
			},
		})

		return res.status(200).json({ message: "User rejected" });

	} catch (error) {
		return res.status(500).json({ message: error });
	}

}

export const getCurrentLoggedUser = async (req: any, res: any, next: any) => {

	try {
		const { id } = req.user;

		// Check if user exists
		const existingUser = await prisma.user.findUnique({
			where: {
				id: id,
			},
		});

		if (!existingUser) {
			return res.status(404).json({ message: "User doesn't exist" });
		}

		const returnedUser= {
			id: existingUser.id,
			role: existingUser.role,
			firstname: existingUser.firstname,
			lastname: existingUser.lastname,
			email: existingUser.email,
			siret: existingUser.siret || '',
			avatar: existingUser.avatar,
			status: existingUser.status,
		}

		return res.status(200).json({ message: "User found", data:{ user:{...returnedUser} }});

	} catch (error) {
		return res.status(500).json({ message: error });
	}

}

export const getCurrentLoggedUserAndCheckAdminRole = async (req: any, res: any, next: any) => {

	try {
		const { id } = req.user;

		// Check if user exists
		const existingUser = await prisma.user.findUnique({
			where: {
				id: id,
			},
		});

		if (!existingUser) {
			return res.status(404).json({ message: "User doesn't exist" });
		}

		if(existingUser.role !== "admin"){
			return res.status(401).json({ message: "Unauthorized" });
		}

		const returnedUser= {
			id: existingUser.id,
			role: existingUser.role,
			firstname: existingUser.firstname,
			lastname: existingUser.lastname,
			email: existingUser.email,
			siret: existingUser.siret || '',
			avatar: existingUser.avatar,
			status: existingUser.status,
		}

		return res.status(200).json({ message: "User found", data:{ user:{...returnedUser} }});

	} catch (error) {
		return res.status(500).json({ message: error });
	}

}

export const getAllUsers_Admin = async (req: any, res: any, next: any) => {
	console.log('test')
	// Check if rq user is admin
	if(req.user.role !== "admin"){
		return res.status(401).json({ message: "Unauthorized" });
	}

	try {
		const users = await prisma.user.findMany({
			select: {
				id: true,
				role: true,
				firstname: true,
				lastname: true,
				email: true,
				siret: true,
				avatar: true,
				status: true,
				createdAt: true,
				guardsRequested: true,
				guardsMade: true,
				applications: true,
				advices: true,
				messages: true,
			},
		});

		return res.status(200).json({ message: "Users found", data:{ users } });

	} catch (error) {
		return res.status(500).json({ message: error });
	}

}

export const getUser = async (req: any, res: any, next: any) => {

	try {
		const { id } = req.params;

		// Check if user exists
		const existingUser = await prisma.user.findUnique({
			where: {
				id: id,
			},
		});

		if (!existingUser) {
			return res.status(404).json({ message: "User doesn't exist" });
		}

		const returnedUser= {
			id: existingUser.id,
			role: existingUser.role,
			firstname: existingUser.firstname,
			lastname: existingUser.lastname,
			email: existingUser.email,
			siret: existingUser.siret || '',
			avatar: existingUser.avatar,
			status: existingUser.status,
			createdAt: existingUser.createdAt,
		}

		return res.status(200).json({ message: "User found", data:{ user:{...returnedUser} }});

	} catch (error) {
		return res.status(500).json({ message: error });
	}

}