import { PrismaClient } from "@prisma/client";
import { convertDateToISO } from "../utils/date";
const bcrypt = require("bcrypt");
const prisma = new PrismaClient();

export const addVisit = async (req: any, res: any, next: any) => {

  const user = req.user
  const guardId = req.params.guardId
  const { date, comment } = req.body

  // Check if data is provided in req
  if(!guardId) {
    return res.status(400).json({message: "Guard ID is required"})
  }

  if(!date) {
    return res.status(400).json({message: "Date is required"})
  }

  if(!req.files){
    return res.status(400).json({message: "Plants images are required"})
  }

  // Get guard
  const guard = await prisma.guard.findUnique({
    where: {
      id: guardId
    },
    include :{
      plants: true
    }
  })

  if(!guard) {
    return res.status(404).json({message: "Guard not found"})
  }

  // Check if user is allowed to add visit
  if(user.id !== guard.guardianId) {
    return res.status(403).json({message: "You are not allowed to add a visit to this guard"})
  }
  
  // Check if date is valid (DD-MM-YYYY)
  const dateParts = date.split("-");
  if (dateParts.length !== 3) {
    return res.status(400).json({message: "Invalid date format"});  
  }

  // Check if there enough files
  if(req.files.plantImage.length !== guard.plants.length) {
    return res.status(400).json({message: "Invalid number of files"})
  }

  try {

    // plantVisits array
    
    const plantVisits = []

    for(let i = 0; i < req.files.plantImage.length; i++) {
      const plant = guard.plants[i]
      const plantImage = req.files.plantImage[i]
      
      plantVisits.push({
        plantId: plant.id,
        image: plantImage.filename
      })
    }

    // Create visit
    const visit = await prisma.visit.create({
      data: {
        guardId: guard.id,
        date: convertDateToISO(date),
        comment: comment,
        plants: {
          create: plantVisits      
        }
      }
    })

    if(!visit) {
      return res.status(500).json({message: "Internal server error"})
    }

    return res.status(201).json({message: "Visit created", data: {visit}})
    
  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})    
  }




}

export const getGuardVisits = async (req: any, res: any, next: any) => {

  const user = req.user
  const guardId = req.params.guardId

  if(!guardId) {
    return res.status(400).json({message: "Guard ID is required"})
  }

  try {
    
    const guard = await prisma.guard.findUnique({
      where: {
        id: guardId
      },
      include: {
        visits: {
          include: {
            plants: {
              include: {
                plant: true
              }
            }
          }
        }
      }
    })

    if(!guard) {
      return res.status(404).json({message: "Guard not found"})
    }

    const isUserInGuard = user.id === guard.ownerId || user.id === guard.guardianId
    const isUserAdminOrBotanist = user.role === "admin" || user.role === "botanist"

    if(!isUserInGuard || (!isUserAdminOrBotanist && !isUserInGuard)) {
      return res.status(403).json({message: "You are not allowed to access this resource"})
    }

    return res.status(200).json({message: "Guard visits found", data: {visits: guard.visits}})

  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})
  }

}

export const getVisit = async (req: any, res: any, next: any) => {

  const user = req.user
  const visitId = req.params.visitId

  if(!visitId) {
    return res.status(400).json({message: "Visit ID is required"})
  }

  try {
    
    const visit = await prisma.visit.findUnique({
      where: {
        id: visitId
      },
      include: {
        guard: {
          include: {
            owner: true,
            guardian: true,
            plants: true
          }
        },
        plants: {
          include: {
            plant: true
          }
        
        }
      }
    })

    if(!visit) {
      return res.status(404).json({message: "Visit not found"})
    }

    const isUserInGuard = user.id === visit.guard.ownerId || user.id === visit.guard.guardianId
    const isUserAdminOrBotanist = user.role === "admin" || user.role === "botanist"

    if(!isUserInGuard || (!isUserAdminOrBotanist && !isUserInGuard)) {
      return res.status(403).json({message: "You are not allowed to access this resource"})
    }

    return res.status(200).json({message: "Visit found", data: {visit}})

  } catch (error) {
    console.log(error)
    return res.status(500).json({message: "Internal server error"})
  }

}