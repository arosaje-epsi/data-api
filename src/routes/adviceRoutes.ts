import { addGuardAdvice, addVisitAdvice, getGuardAdvices, getVisitAdvices } from "../controllers/adviceController";
import { isAuthenticated } from "../utils/jwt";

const adviceRouter = require("express").Router();

adviceRouter.post('/guard/add', isAuthenticated, addGuardAdvice)
adviceRouter.post('/visit/add', isAuthenticated, addVisitAdvice)
adviceRouter.get('/guard/:guardId', isAuthenticated, getGuardAdvices)
adviceRouter.get('/visit/:visitId', isAuthenticated, getVisitAdvices)

module.exports = adviceRouter;