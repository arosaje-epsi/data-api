import { addApply, addGuard, avaiablesGuards, confirmApply, getAllGuards, getGuardDetails, getGuardsByUserId, removeApply, updateGuard, userGuards } from "../controllers/guardController";
import { isAuthenticated } from "../utils/jwt";
const multer = require('../utils/multer');

const guardRouter = require("express").Router();

guardRouter.post('/add', isAuthenticated, multer.fields([{name:'plantImage', maxCount:20}]), addGuard)
guardRouter.post('/edit/:guardId', isAuthenticated, updateGuard)
guardRouter.post('/availables', isAuthenticated, avaiablesGuards)
guardRouter.get('/userGuards', isAuthenticated, userGuards)
guardRouter.get('/allGuards', isAuthenticated, getAllGuards)
guardRouter.get('/user/:userId', isAuthenticated, getGuardsByUserId)
guardRouter.get('/detail/:guardId', isAuthenticated, getGuardDetails)

guardRouter.get('/apply/:guardId', isAuthenticated, addApply)
guardRouter.get('/apply/remove/:applicationId', isAuthenticated, removeApply)
guardRouter.get('/apply/confirm/:applicationId', isAuthenticated, confirmApply)

module.exports = guardRouter;