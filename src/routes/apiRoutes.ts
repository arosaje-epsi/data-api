const router = require("express").Router();
const userRoute = require("./userRoutes");
const adviceRoutes = require("./adviceRoutes");
const guardRoutes = require("./guardRoutes");
const visitRoutes = require("./visitRoutes");
const plantRoutes = require("./plantRoutes");


router.use('/user', userRoute)
router.use('/guard', guardRoutes)
router.use('/visit', visitRoutes)
router.use('/advice', adviceRoutes)
router.use('/plant', plantRoutes)

module.exports = router;