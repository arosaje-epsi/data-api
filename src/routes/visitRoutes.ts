import { isAuthenticated } from "../utils/jwt";
import { addVisit, getGuardVisits, getVisit } from "../controllers/visitController";
const multer = require('../utils/multer');

const visitRouter = require("express").Router();

visitRouter.post('/add/:guardId', isAuthenticated, multer.fields([{name:'plantImage', maxCount:20}]), addVisit);

visitRouter.get('/guard/:guardId', isAuthenticated, getGuardVisits)
visitRouter.get('/:visitId', isAuthenticated, getVisit)

module.exports = visitRouter;