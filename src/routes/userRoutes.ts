import { isAuthenticated } from "../utils/jwt";
import { getAllUsers_Admin, getCurrentLoggedUser, getCurrentLoggedUserAndCheckAdminRole, getUser, setBotanistAccepted, setBotanistRejected, userBan, userCreate, userDelete, userGrant, userPending } from "../controllers/userController";

const userRouter = require("express").Router();

userRouter.post('/register', userCreate)
userRouter.post('/ban', isAuthenticated, userBan)
userRouter.post('/grant', isAuthenticated, userGrant)
userRouter.post('/botanist/grant', isAuthenticated, setBotanistAccepted)
userRouter.post('/botanist/reject', isAuthenticated, setBotanistRejected)

userRouter.get('/delete/:id', isAuthenticated, userDelete)
userRouter.get('/current', getCurrentLoggedUser)
userRouter.get('/current/isAdmin', getCurrentLoggedUserAndCheckAdminRole)

// ###

userRouter.get('/all', isAuthenticated, getAllUsers_Admin)
userRouter.get('/:id', isAuthenticated, getUser)
userRouter.post('/pending', isAuthenticated, userPending)

module.exports = userRouter;