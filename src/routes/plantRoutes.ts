import { getTypes } from "../controllers/plantController";

const plantRouter = require("express").Router();

plantRouter.get('/getTypes', getTypes)

module.exports = plantRouter;