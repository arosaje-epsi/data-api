FROM node:18
WORKDIR /usr/src/app
COPY package*.json ./
RUN rm -rf node_modules
RUN npm install
COPY . .
RUN npx prisma generate
RUN npm run build
EXPOSE 2000
CMD [ "npm", "run", "start" ]