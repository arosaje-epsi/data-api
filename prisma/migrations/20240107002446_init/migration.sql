/*
  Warnings:

  - You are about to drop the column `photo` on the `PlantVisit` table. All the data in the column will be lost.
  - You are about to drop the column `photo` on the `Plant` table. All the data in the column will be lost.
  - Added the required column `image` to the `PlantVisit` table without a default value. This is not possible if the table is not empty.
  - Added the required column `image` to the `Plant` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_PlantVisit" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "visitId" TEXT NOT NULL,
    "plantId" TEXT NOT NULL,
    "image" TEXT NOT NULL,
    CONSTRAINT "PlantVisit_visitId_fkey" FOREIGN KEY ("visitId") REFERENCES "Visit" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "PlantVisit_plantId_fkey" FOREIGN KEY ("plantId") REFERENCES "Plant" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_PlantVisit" ("id", "plantId", "visitId") SELECT "id", "plantId", "visitId" FROM "PlantVisit";
DROP TABLE "PlantVisit";
ALTER TABLE "new_PlantVisit" RENAME TO "PlantVisit";
CREATE TABLE "new_Plant" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "guardId" TEXT,
    "type" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "image" TEXT NOT NULL,
    CONSTRAINT "Plant_guardId_fkey" FOREIGN KEY ("guardId") REFERENCES "Guard" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_Plant" ("guardId", "id", "name", "type") SELECT "guardId", "id", "name", "type" FROM "Plant";
DROP TABLE "Plant";
ALTER TABLE "new_Plant" RENAME TO "Plant";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
