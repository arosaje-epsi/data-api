/*
  Warnings:

  - Added the required column `referer` to the `Advice` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Advice" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "userId" TEXT NOT NULL,
    "referer" TEXT NOT NULL,
    "guardId" TEXT,
    "visitId" TEXT,
    "content" TEXT NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "Advice_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Advice_guardId_fkey" FOREIGN KEY ("guardId") REFERENCES "Guard" ("id") ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT "Advice_visitId_fkey" FOREIGN KEY ("visitId") REFERENCES "Visit" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_Advice" ("content", "createdAt", "guardId", "id", "userId", "visitId") SELECT "content", "createdAt", "guardId", "id", "userId", "visitId" FROM "Advice";
DROP TABLE "Advice";
ALTER TABLE "new_Advice" RENAME TO "Advice";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
